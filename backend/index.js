const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

//middleware
app.use(bodyParser.json());
app.use(cors());

const reports = require('./routes/api/reports') ;
app.use('/api/reports', reports);

//handle production code
if(process.env.NODE_ENV === 'production') {
  //static folder
  app.use(express.static(__dirname + '/public/'));

  //handle single page application requests
  app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.html'))
}

const port = process.env.PORT || 4000;

app.listen(port, () => console.log(`Server running on port: ${port}`));
