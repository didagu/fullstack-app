
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config({ path:'backend/.env'});
}

const dbUsername = process.env.DB_USERNAME;
const dbPassword = process.env.DB_PASSWORD;
const clusterEndpoint = process.env.CLUSTER_ENDPOINT;

const MongoClient = require('mongodb').MongoClient;
const uri = `mongodb+srv://${dbUsername}:${dbPassword}@${clusterEndpoint}/test?retryWrites=true&w=majority`;
const client = new MongoClient(uri, { useNewUrlParser: true });
client.connect();

module.exports = client;
